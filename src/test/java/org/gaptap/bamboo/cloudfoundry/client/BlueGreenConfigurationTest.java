package org.gaptap.bamboo.cloudfoundry.client;

import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class BlueGreenConfigurationTest {

    @Test
    public void ifEnabledIsNotSetItIsDefaultedToFalse(){
        BlueGreenConfiguration configuration = BlueGreenConfiguration.builder()
                .build();

        assertFalse(configuration.isEnabled());
    }

    @Test
    public void ifEnabledIsNullItIsDefaultedToFalse(){
        BlueGreenConfiguration configuration = BlueGreenConfiguration.builder()
                .enabled(null)
                .build();

        assertFalse(configuration.isEnabled());
    }
    @Test
    public void ifTheHealthCheckEndpointIsSetTheHealthCheckIsEnabled(){
        BlueGreenConfiguration configuration = BlueGreenConfiguration.builder()
                .enabled(true)
                .healthCheckEndpoint("/health")
                .build();

        assertTrue(configuration.healthCheckEnabled());
    }

    @Test
    public void ifTheHealthCheckEndpointIsNotSetTheHealthCheckIsDisabled(){
        BlueGreenConfiguration configuration = BlueGreenConfiguration.builder()
                .enabled(true)
                .build();

        assertFalse(configuration.healthCheckEnabled());
    }

    @Test
    public void ifTheHealthCheckEndpointIsSetToAnEmptyStringTheHealthCheckIsDisabled(){
        BlueGreenConfiguration configuration = BlueGreenConfiguration.builder()
                .enabled(true)
                .healthCheckEndpoint("")
                .build();

        assertFalse(configuration.healthCheckEnabled());
    }

    @Test
    public void convertToDarkAppConfigurationIsTheSameAsTheNameWhenABlueGreenDeploymentIsNotEnabled(){
        ApplicationConfiguration applicationConfiguration = ApplicationConfiguration.builder()
                .name("test-app")
                .build();
        BlueGreenConfiguration blueGreenConfiguration = BlueGreenConfiguration.builder()
                .enabled(false)
                .build();

        ApplicationConfiguration convertedConfiguration = blueGreenConfiguration.convertToDarkAppConfiguration(applicationConfiguration);

        assertThat(convertedConfiguration.name(), is(applicationConfiguration.name()));
    }

    @Test
    public void convertToDarkAppConfigurationNameHasDashDarkAppendedWhenABlueGreenDeploymentIsEnabledAndACustomDarkNameIsNotProvided(){
        ApplicationConfiguration applicationConfiguration = ApplicationConfiguration.builder()
                .name("test-app")
                .build();
        BlueGreenConfiguration blueGreenConfiguration = BlueGreenConfiguration.builder()
                .enabled(true)
                .build();

        ApplicationConfiguration convertedConfiguration = blueGreenConfiguration.convertToDarkAppConfiguration(applicationConfiguration);

        assertThat(convertedConfiguration.name(), is(applicationConfiguration.name() + "-dark"));
    }

    @Test
    public void convertToDarkAppConfigurationNameIsTheCustomDarkNameWhenABlueGreenDeploymentHasACustomNameProvided() {
        ApplicationConfiguration applicationConfiguration = ApplicationConfiguration.builder()
                .name("test-app")
                .build();
        BlueGreenConfiguration blueGreenConfiguration = BlueGreenConfiguration.builder()
                .enabled(true)
                .useCustomDarkAppConfiguration(true)
                .customDarkAppName("custom-name")
                .build();

        ApplicationConfiguration convertedConfiguration = blueGreenConfiguration.convertToDarkAppConfiguration(applicationConfiguration);

        assertThat(convertedConfiguration.name(), is(blueGreenConfiguration.customDarkAppName()));
    }

    @Test
    public void convertToDarkAppConfigurationRoutesIsTheCustomDarkRouteWhenABlueGreenDeploymentHasACustomRouteProvided(){
        ApplicationConfiguration applicationConfiguration = ApplicationConfiguration.builder()
                .name("test-app")
                .route("live.cf.com")
                .build();
        BlueGreenConfiguration blueGreenConfiguration = BlueGreenConfiguration.builder()
                .enabled(true)
                .useCustomDarkAppConfiguration(true)
                .customDarkRoute("custom-dark.cf.com")
                .build();

        ApplicationConfiguration convertedConfiguration = blueGreenConfiguration.convertToDarkAppConfiguration(applicationConfiguration);

        assertThat(convertedConfiguration.routes().get(0), is(blueGreenConfiguration.customDarkRoute()));
    }

    @Test
    public void convertToDarkAppConfigurationRouteHasDashDarkAppendedToAllHostsWhenABlueGreenDeploymentIsEnabledAndACustomDarkRouteIsNotProvided(){
        ApplicationConfiguration applicationConfiguration = ApplicationConfiguration.builder()
                .name("test-app")
                .route("live.cf.com", "another.cf.com")
                .build();
        BlueGreenConfiguration blueGreenConfiguration = BlueGreenConfiguration.builder()
                .enabled(true)
                .build();

        ApplicationConfiguration convertedConfiguration = blueGreenConfiguration.convertToDarkAppConfiguration(applicationConfiguration);

        assertThat(convertedConfiguration.routes().get(0), is("live-dark.cf.com"));
        assertThat(convertedConfiguration.routes().get(1), is("another-dark.cf.com"));
    }

    @Test
    public void convertToDarkAppConfigurationRouteHasDashDarkAppendedToRouteWithPathWhenABlueGreenDeploymentIsEnabledAndACustomDarkRouteIsNotProvided(){
        ApplicationConfiguration applicationConfiguration = ApplicationConfiguration.builder()
                .name("test-app")
                .route("live.cf.com/test-path")
                .build();
        BlueGreenConfiguration blueGreenConfiguration = BlueGreenConfiguration.builder()
                .enabled(true)
                .healthCheckEndpoint("/health")
                .build();

        ApplicationConfiguration convertedConfiguration = blueGreenConfiguration.convertToDarkAppConfiguration(applicationConfiguration);

        assertThat(convertedConfiguration.routes().get(0), is("live-dark.cf.com/test-path"));
    }

    @Test
    public void ifABlueGreenDeploymentIsNotConfiguredTheConvertedRoutesAreTheSameAsTheRoutes() {
        ApplicationConfiguration applicationConfiguration = ApplicationConfiguration.builder()
                .name("test-app")
                .route("live.cf.com", "another.cf.com")
                .build();
        BlueGreenConfiguration blueGreenConfiguration = BlueGreenConfiguration.builder()
                .enabled(false)
                .build();

        ApplicationConfiguration convertedConfiguration = blueGreenConfiguration.convertToDarkAppConfiguration(applicationConfiguration);

        assertThat(convertedConfiguration.routes().get(0), is("live.cf.com"));
        assertThat(convertedConfiguration.routes().get(1), is("another.cf.com"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void ifUseCustomDarkAppConfigurationIsFalseCustomDarkAppNameCanNotBeProvided(){
        BlueGreenConfiguration blueGreenConfiguration = BlueGreenConfiguration.builder()
                .enabled(false)
                .useCustomDarkAppConfiguration(false)
                .customDarkAppName("fail")
                .build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void ifUseCustomDarkAppConfigurationIsFalseCustomDarkRouteCanNotBeProvided(){
        BlueGreenConfiguration blueGreenConfiguration = BlueGreenConfiguration.builder()
                .enabled(false)
                .useCustomDarkAppConfiguration(false)
                .customDarkRoute("fail.com")
                .build();
    }
}
