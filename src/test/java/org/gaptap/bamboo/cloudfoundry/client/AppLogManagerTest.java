/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.client;

import org.cloudfoundry.doppler.Envelope;
import org.cloudfoundry.doppler.EventType;
import org.cloudfoundry.doppler.LogMessage;
import org.cloudfoundry.doppler.MessageType;
import org.cloudfoundry.doppler.StreamRequest;
import org.cloudfoundry.operations.CloudFoundryOperations;
import org.cloudfoundry.operations.applications.ApplicationDetail;
import org.cloudfoundry.operations.applications.GetApplicationRequest;
import org.junit.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.TimeZone;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class AppLogManagerTest extends AbstractJavaClientTest {

    private static void requestGetApplication(CloudFoundryOperations cloudFoundryOperations, String applicationId, String applicationName){
        when(cloudFoundryOperations
                .applications()
                .get(GetApplicationRequest.builder()
                        .name(applicationName)
                        .build()))
                        .thenReturn(Mono
                                        .just(ApplicationDetail.builder()
                                                        .name(applicationName)
                                                        .id(applicationId)
                                                        .stack("cflinuxfs2")
                                                        .diskQuota(1073741824)
                                                        .instances(2)
                                                        .memoryLimit(1024)
                                                        .requestedState("started")
                                                        .runningInstances(2)
                                                        .build()
                                        )
                        );
    }

    protected  Flux<Envelope> getLogPublisher() {
        return Flux.just(
                Envelope.builder()
                        .deployment("cf-cfapps-io2-diego")
                        .eventType(EventType.LOG_MESSAGE)
                        .index("33")
                        .ip("10.10.115.68")
                        .job("cell_z2")
                        .logMessage(LogMessage.builder()
                                .applicationId("1a95eadc-95c6-4675-aa07-8c02f80ea8a4")
                                .message("2016-04-21 22:36:28.035  INFO 24 --- [           main] o.s.j.e.a.AnnotationMBeanExporter        : Located managed bean 'rabbitConnectionFactory': registering with " +
                                        "JMX server as MBean [org.springframework.amqp.rabbit.connection:name=rabbitConnectionFactory,type=CachingConnectionFactory]")
                                .messageType(MessageType.OUT)
                                .sourceInstance("0")
                                .sourceType("APP")
                                .timestamp(1461278188035928339L)
                                .build())
                        .origin("rep")
                        .timestamp(1461278188035930425L)
                        .build(),
                Envelope.builder()
                        .deployment("cf-cfapps-io2-diego")
                        .eventType(EventType.LOG_MESSAGE)
                        .index("33")
                        .ip("10.10.115.68")
                        .job("cell_z2")
                        .logMessage(LogMessage.builder()
                                .applicationId("1a95eadc-95c6-4675-aa07-8c02f80ea8a4")
                                .message("Container became healthy")
                                .messageType(MessageType.OUT)
                                .sourceInstance("0")
                                .sourceType("CELL")
                                .timestamp(1461278188715651492L)
                                .build())
                        .origin("rep")
                        .timestamp(1461278188715653514L)
                        .build());
    }

    @Test
    public void logFormatting() throws InterruptedException {
        // Given
        requestGetApplication(cloudFoundryOperations, "1", "my-app");

        when(dopplerClient.stream(StreamRequest.builder()
                .applicationId("1")
                .build()))
        .thenReturn(getLogPublisher());

        // When
        AppLogManager appLogManager = new AppLogManager(cloudFoundryOperations, dopplerClient, TimeZone.getTimeZone("America/New_York"));
        AccumulatingLogger logger = new AccumulatingLogger();
        appLogManager.startTailingLogs("my-app", logger);

        Thread.sleep(500);

        assertTrue(logger.containsInfoLogEntry("2016-04-21T18:36.28-0400 [APP/0]      OUT 2016-04-21 22:36:28.035  INFO 24 --- [           main] o.s.j.e.a.AnnotationMBeanExporter        : Located managed bean 'rabbitConnectionFactory': registering with JMX server as MBean [org.springframework.amqp.rabbit.connection:name=rabbitConnectionFactory,type=CachingConnectionFactory]"));
        assertTrue(logger.containsInfoLogEntry("2016-04-21T18:36.28-0400 [CELL/0]     OUT Container became healthy"));
    }
}
