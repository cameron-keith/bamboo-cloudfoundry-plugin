/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.client;

import java.util.ArrayList;
import java.util.List;

public class AccumulatingLogger implements Logger {

    private static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(AccumulatingLogger.class);

    private final List<String> infoLogs = new ArrayList<>();
    private final List<String> warnLogs = new ArrayList<>();
    private final List<String> errorLogs = new ArrayList<>();

    @Override
    public void info(String message) {
        infoLogs.add(message);
    }

    @Override
    public void warn(String message) {
        warnLogs.add(message);
    }

    @Override
    public void error(String message) {
        errorLogs.add(message);
    }

    public boolean containsInfoLogEntry(String message) {
        if(!infoLogs.contains(message)){
            LOG.info("Info Logs didn't contain the given message. Info logs were the following:");
            infoLogs.stream().forEach(log -> LOG.info("<<" + log + ">>"));
            return false;
        }
        return true;
    }

    public boolean containsWarnLogEntry(String message) {
        if(!warnLogs.contains(message)){
            LOG.info("Warn Logs didn't contain the given message. Warn logs were the following:");
            warnLogs.stream().forEach(log -> LOG.info("<<" + log + ">>"));
            return false;
        }
        return true;
    }
}
