package org.gaptap.bamboo.cloudfoundry.client;

import org.junit.Test;
import org.springframework.web.util.UriComponents;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class ReactorHealthCheckerTest {

    @Test(expected = IllegalArgumentException.class)
    public void anAppMustHaveRoutesForABlueGreenHealthCheckToBePerformed(){
        ApplicationConfiguration applicationConfiguration = ApplicationConfiguration.builder()
                .name("test-app")
                .build();
        BlueGreenConfiguration blueGreenConfiguration = BlueGreenConfiguration.builder()
                .enabled(true)
                .customDarkAppName("green-app")
                .customDarkRoute("green.cf.com")
                .healthCheckEndpoint("/health")
                .build();
        HealthChecker healthChecker = new ReactorHealthChecker();

        healthChecker.assertHealthy(applicationConfiguration, blueGreenConfiguration, new Log4jLogger());
    }

    @Test
    public void anAppWithAPathRouteWillCorrectlyFormatTheUrl(){
        ApplicationConfiguration applicationConfiguration = ApplicationConfiguration.builder()
                .name("test-app")
                .route("live.cf.com/test-path")
                .build();
        BlueGreenConfiguration blueGreenConfiguration = BlueGreenConfiguration.builder()
                .enabled(true)
                .healthCheckEndpoint("/health")
                .build();
        ReactorHealthChecker healthChecker = new ReactorHealthChecker();
        UriComponents uriComponents = healthChecker.getUriComponents(applicationConfiguration, blueGreenConfiguration);

        assertThat(uriComponents.toString(), is("https://live.cf.com:443/test-path/health"));
    }
}
