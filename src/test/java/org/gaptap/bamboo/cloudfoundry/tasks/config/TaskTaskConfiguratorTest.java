package org.gaptap.bamboo.cloudfoundry.tasks.config;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.collections.SimpleActionParametersMap;
import com.atlassian.bamboo.task.TaskDefinition;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.gaptap.bamboo.cloudfoundry.tasks.config.TaskTaskConfigurator.OPTION_RUN_TASK;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.TaskTaskConfigurator.OPTION_WAIT_ON_TASK;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.TaskTaskConfigurator.RUN_APPLICATION_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.TaskTaskConfigurator.RUN_CAPTURE_TASK_ID;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.TaskTaskConfigurator.RUN_COMMAND;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.TaskTaskConfigurator.RUN_ENVIRONMENT;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.TaskTaskConfigurator.RUN_MEMORY;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.TaskTaskConfigurator.RUN_TASK_ID_VARIABLE_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.TaskTaskConfigurator.RUN_TASK_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.TaskTaskConfigurator.SELECTED_OPTION;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.TaskTaskConfigurator.WAIT_APPLICATION_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.TaskTaskConfigurator.WAIT_FAIL_ON_TASK_FAILURE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.TaskTaskConfigurator.WAIT_TASK_ID;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.TaskTaskConfigurator.WAIT_TERMINATE_TASK_ON_TIMEOUT;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.TaskTaskConfigurator.WAIT_TIMEOUT;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.RETURNS_SMART_NULLS;
import static org.mockito.Mockito.mock;

public class TaskTaskConfiguratorTest extends BaseCloudFoundryTaskConfiguratorTest {

    private TaskTaskConfigurator taskConfigurator;

    private Map<String, Object> params;
    private ActionParametersMap actionParametersMap;

    @Before
    public void setup(){
        taskConfigurator = new TaskTaskConfigurator(adminService, textProvider, taskConfiguratorHelper, encryptionService, cloudFoundryServiceFactory);

        params = getBaseRequiredParameters();
        actionParametersMap = new SimpleActionParametersMap(params);
    }

    @Test
    public void allParametersAreSavedInTheConfigMap(){
        params.put(SELECTED_OPTION, OPTION_RUN_TASK);
        params.put(RUN_APPLICATION_NAME, "test-app");
        params.put(RUN_COMMAND, "run.sh");
        params.put(RUN_TASK_NAME, "test-task");
        params.put(RUN_MEMORY, "512");
        params.put(RUN_ENVIRONMENT, "ENV:VALUE");
        params.put(RUN_CAPTURE_TASK_ID, "true");
        params.put(RUN_TASK_ID_VARIABLE_NAME, "cf");
        params.put(WAIT_TERMINATE_TASK_ON_TIMEOUT, "true");
        params.put(WAIT_APPLICATION_NAME, "my-app");
        params.put(WAIT_TASK_ID, "wait-task");
        params.put(WAIT_TIMEOUT, "150");
        params.put(WAIT_FAIL_ON_TASK_FAILURE, "true");
        Map<String, String> taskConfigMap = taskConfigurator.generateTaskConfigMap(actionParametersMap, previousTaskDefinition);

        assertThat(taskConfigMap.get(SELECTED_OPTION), is(OPTION_RUN_TASK));
        assertThat(taskConfigMap.get(RUN_APPLICATION_NAME), is("test-app"));
        assertThat(taskConfigMap.get(RUN_COMMAND), is("run.sh"));
        assertThat(taskConfigMap.get(RUN_TASK_NAME), is("test-task"));
        assertThat(taskConfigMap.get(RUN_MEMORY), is("512"));
        assertThat(taskConfigMap.get(RUN_ENVIRONMENT), is("ENV:VALUE"));
        assertThat(taskConfigMap.get(RUN_CAPTURE_TASK_ID), is("true"));
        assertThat(taskConfigMap.get(RUN_TASK_ID_VARIABLE_NAME), is("cf"));
        assertThat(taskConfigMap.get(WAIT_TERMINATE_TASK_ON_TIMEOUT), is("true"));
        assertThat(taskConfigMap.get(WAIT_APPLICATION_NAME), is("my-app"));
        assertThat(taskConfigMap.get(WAIT_TASK_ID), is("wait-task"));
        assertThat(taskConfigMap.get(WAIT_TIMEOUT), is("150"));
        assertThat(taskConfigMap.get(WAIT_FAIL_ON_TASK_FAILURE), is("true"));
    }

    @Test
    public void selectedOptionIsRequired() {
        taskConfigurator.validate(actionParametersMap, errorCollection);

        assertFalse(errorCollection.getErrors().isEmpty());
        assertNotNull(errorCollection.getErrors().get(SELECTED_OPTION));
    }

    @Test
    public void runTaskRequiredFields() {
        params.put(SELECTED_OPTION, OPTION_RUN_TASK);

        taskConfigurator.validate(actionParametersMap, errorCollection);

        assertThat(errorCollection.getErrors().size(), is(4));
        assertNotNull(errorCollection.getErrors().get(RUN_APPLICATION_NAME));
        assertNotNull(errorCollection.getErrors().get(RUN_COMMAND));
        assertNotNull(errorCollection.getErrors().get(RUN_TASK_NAME));
        assertNotNull(errorCollection.getErrors().get(RUN_CAPTURE_TASK_ID));
    }

    @Test
    public void runTaskWhenCaptureTaskIdIsTrueTheBambooVariableNameIsRequired() {
        params.put(SELECTED_OPTION, OPTION_RUN_TASK);
        params.put(RUN_CAPTURE_TASK_ID, "true");

        taskConfigurator.validate(actionParametersMap, errorCollection);

        assertNotNull(errorCollection.getErrors().get(RUN_TASK_ID_VARIABLE_NAME));
    }

    @Test
    public void runTaskBambooVariableNameIsDefaulted() {
        Map<String, Object> context = new HashMap<>();

        taskConfigurator.populateContextForCreate(context);

        assertThat(context.get(RUN_TASK_ID_VARIABLE_NAME), is("cloudfoundry.task.id"));
    }

    @Test
    public void waitTaskRequiredFields() {
        params.put(SELECTED_OPTION, OPTION_WAIT_ON_TASK);

        taskConfigurator.validate(actionParametersMap, errorCollection);

        assertThat(errorCollection.getErrors().size(), is(3));
        assertNotNull(errorCollection.getErrors().get(WAIT_APPLICATION_NAME));
        assertNotNull(errorCollection.getErrors().get(WAIT_TASK_ID));
        assertNotNull(errorCollection.getErrors().get(WAIT_TIMEOUT));
    }

    @Test
    public void waitTaskIdIsDefaultedToABambooVariable() {
        Map<String, Object> context = new HashMap<>();

        taskConfigurator.populateContextForCreate(context);

        assertThat(context.get(WAIT_TASK_ID), is("${bamboo.cloudfoundry.task.id}"));
    }

    @Test
    public void waitTaskFailOnTaskFailureIsDefaultedToTrue() {
        Map<String, Object> context = new HashMap<>();

        taskConfigurator.populateContextForCreate(context);

        assertThat(context.get(WAIT_FAIL_ON_TASK_FAILURE), is("true"));
    }

    @Test
    public void waitTaskTimeoutIsDefaultedFiveMinutes() {
        Map<String, Object> context = new HashMap<>();

        taskConfigurator.populateContextForCreate(context);

        assertThat(context.get(WAIT_TIMEOUT), is("300"));
    }
}
