package org.gaptap.bamboo.cloudfoundry.tasks;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.build.logger.Log4jBuildLogger;
import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.configuration.ConfigurationMapImpl;
import com.atlassian.bamboo.security.EncryptionService;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.v2.build.CommonContext;
import com.atlassian.bamboo.variable.VariableContext;
import com.atlassian.bamboo.variable.VariableDefinitionContext;
import com.google.common.collect.Maps;
import org.gaptap.bamboo.cloudfoundry.client.CloudFoundryService;
import org.gaptap.bamboo.cloudfoundry.client.CloudFoundryServiceFactory;
import org.gaptap.bamboo.cloudfoundry.client.ConnectionParameters;
import org.gaptap.bamboo.cloudfoundry.client.Logger;
import org.junit.Before;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import reactor.core.publisher.Mono;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import static org.gaptap.bamboo.cloudfoundry.tasks.config.BaseCloudFoundryTaskConfigurator.ORGANIZATION;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.BaseCloudFoundryTaskConfigurator.SPACE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.APP_CONFIG_OPTION_YAML;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.APP_LOCATION;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.APP_LOCATION_OPTION_DIRECTORY;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.BLUE_GREEN_ENABLED;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.DIRECTORY;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.SECONDS_DO_NOT_MONITOR;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.SELECTED_APP_CONFIG_OPTION;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.START;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.STARTUP_TIMEOUT;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.YAML_FILE;
import static org.gaptap.bamboo.cloudfoundry.tasks.dataprovider.TargetTaskDataProvider.PASSWORD;
import static org.gaptap.bamboo.cloudfoundry.tasks.dataprovider.TargetTaskDataProvider.TARGET_URL;
import static org.gaptap.bamboo.cloudfoundry.tasks.dataprovider.TargetTaskDataProvider.TRUST_SELF_SIGNED_CERTS;
import static org.gaptap.bamboo.cloudfoundry.tasks.dataprovider.TargetTaskDataProvider.USERNAME;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.RETURNS_SMART_NULLS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public abstract class AbstractTaskTest {

    @Mock
    protected EncryptionService encryptionService;
    @Mock
    protected CloudFoundryServiceFactory cloudFoundryServiceFactory;
    @Mock
    protected CloudFoundryService cloudFoundryService;
    protected CommonTaskContext taskContext;

    protected BuildLogger buildLogger = new Log4jBuildLogger();

    protected ConfigurationMap configurationMap;
    protected Map<String, String> runtimeTaskContext;
    protected VariableContext variableContext;

    @Before
    public void baseInit() {
        MockitoAnnotations.initMocks(this);

        taskContext = mock(CommonTaskContext.class, RETURNS_SMART_NULLS);

        when(taskContext.getRootDirectory()).thenReturn(new File("."));
        when(taskContext.getWorkingDirectory()).thenReturn(new File("."));

        createCommonContext();
        createDefaultRuntimeContext();
        createCloudFoundryService();

        when(taskContext.getBuildLogger()).thenReturn(buildLogger);
    }

    private void createCommonContext() {
        CommonContext commonContext = mock(CommonContext.class);
        when(taskContext.getCommonContext()).thenReturn(commonContext);

        variableContext = mock(VariableContext.class);
        when(commonContext.getVariableContext()).thenReturn(variableContext);

        when(variableContext.getDefinitions()).thenReturn(new HashMap<String, VariableDefinitionContext>());
    }

    private void createCloudFoundryService() {
        when(cloudFoundryServiceFactory.getCloudFoundryService(any(ConnectionParameters.class), any(Logger.class)))
                .thenReturn(cloudFoundryService);
        when(cloudFoundryServiceFactory.getCloudFoundryService(any(ConnectionParameters.class), anyString(), anyString(), any(Logger.class)))
                .thenReturn(cloudFoundryService);
        when(cloudFoundryService.defaultDomain(anyString()))
                .thenReturn(Mono.just("test"));
    }

    private void createDefaultRuntimeContext() {
        runtimeTaskContext = Maps.newHashMap();
        when(taskContext.getRuntimeTaskContext())
                .thenReturn(runtimeTaskContext);
        runtimeTaskContext.put(TARGET_URL, "https://api.run.pivotal.io");
        runtimeTaskContext.put(USERNAME, "username");
        runtimeTaskContext.put(PASSWORD, "password");
        runtimeTaskContext.put(TRUST_SELF_SIGNED_CERTS, "true");
        runtimeTaskContext.put(ORGANIZATION, "org");
        runtimeTaskContext.put(SPACE, "space");
    }
}
