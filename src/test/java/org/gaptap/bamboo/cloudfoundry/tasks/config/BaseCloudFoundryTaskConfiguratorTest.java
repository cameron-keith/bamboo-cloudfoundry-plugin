/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.tasks.config;

import com.atlassian.bamboo.build.BuildDefinitionManager;
import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.collections.SimpleActionParametersMap;
import com.atlassian.bamboo.deployments.environments.service.EnvironmentService;
import com.atlassian.bamboo.security.EncryptionService;
import com.atlassian.bamboo.task.TaskConfigurator;
import com.atlassian.bamboo.task.TaskConfiguratorHelper;
import com.atlassian.bamboo.task.TaskConfiguratorHelperImpl;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.bamboo.utils.error.SimpleErrorCollection;
import com.atlassian.struts.TextProvider;
import org.gaptap.bamboo.cloudfoundry.admin.CloudFoundryAdminService;
import org.gaptap.bamboo.cloudfoundry.client.CloudFoundryServiceFactory;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashMap;
import java.util.Map;

import static org.gaptap.bamboo.cloudfoundry.tasks.config.BaseCloudFoundryTaskConfigurator.ENVIRONMENT_CONFIG_OPTION;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.BaseCloudFoundryTaskConfigurator.ENVIRONMENT_CONFIG_OPTION_SHARED;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.BaseCloudFoundryTaskConfigurator.ENVIRONMENT_CONFIG_OPTION_TASK;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.BaseCloudFoundryTaskConfigurator.ENVIRONMENT_IS_PASSWORD_ENCRYPTED;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.BaseCloudFoundryTaskConfigurator.ENVIRONMENT_PASSWORD;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.BaseCloudFoundryTaskConfigurator.ENVIRONMENT_PASSWORD_ENCRYPTED;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.BaseCloudFoundryTaskConfigurator.ENVIRONMENT_PROXY_HOST;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.BaseCloudFoundryTaskConfigurator.ENVIRONMENT_PROXY_PORT;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.BaseCloudFoundryTaskConfigurator.ENVIRONMENT_URL;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.BaseCloudFoundryTaskConfigurator.ENVIRONMENT_USERNAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.BaseCloudFoundryTaskConfigurator.ORGANIZATION;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.BaseCloudFoundryTaskConfigurator.PASSWORD_CHANGE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.BaseCloudFoundryTaskConfigurator.SPACE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.BaseCloudFoundryTaskConfigurator.TARGET_ID;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.UserProvidedServiceTaskConfigurator.DATA_OPTION_INLINE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.UserProvidedServiceTaskConfigurator.INLINE_DATA;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.UserProvidedServiceTaskConfigurator.SELECTED_DATA_OPTION;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.UserProvidedServiceTaskConfigurator.SERVICE_NAME;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.RETURNS_SMART_NULLS;
import static org.mockito.Mockito.mock;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.when;

public class BaseCloudFoundryTaskConfiguratorTest {

    @Mock
    protected CloudFoundryAdminService adminService;
    @Mock
    protected TextProvider textProvider;
    @Mock
    protected BuildDefinitionManager buildDefinitionManager;
    @Mock
    protected EnvironmentService environmentService;
    @Mock
    protected EncryptionService encryptionService;
    @Mock
    protected CloudFoundryServiceFactory cloudFoundryServiceFactory;

    protected TaskDefinition previousTaskDefinition;

    protected TaskConfiguratorHelper taskConfiguratorHelper;
    protected ErrorCollection errorCollection;

    @Before
    public final void setupBaseTest(){
        MockitoAnnotations.initMocks(this);

        previousTaskDefinition = mock(TaskDefinition.class, RETURNS_SMART_NULLS);

        taskConfiguratorHelper = new TaskConfiguratorHelperImpl(buildDefinitionManager, environmentService, textProvider);

        errorCollection = new SimpleErrorCollection();
    }

    protected Map<String, Object> getBaseRequiredParameters() {
        Map<String, Object> params = new HashMap<>();
        params.put(ENVIRONMENT_CONFIG_OPTION, ENVIRONMENT_CONFIG_OPTION_SHARED);
        params.put(TARGET_ID, "1");
        params.put(ORGANIZATION, "dehringer");
        params.put(SPACE, "sandbox");
        return params;
    }

    protected static void assertTaskValidationHasNoErrors(TaskConfigurator taskConfigurator, ActionParametersMap actionParametersMap, ErrorCollection errorCollection) {
        taskConfigurator.validate(actionParametersMap, errorCollection);

        assertTrue(errorCollection.getErrors().isEmpty());
    }

    @Test
    public void theCustomCfEnvironmentParametersAcceptBambooVariableInTheValues(){
        Map<String, Object> params = getBaseRequiredParameters();
        params.put(ENVIRONMENT_CONFIG_OPTION, ENVIRONMENT_CONFIG_OPTION_TASK);
        params.put(ENVIRONMENT_URL, "${bamboo.cf.environment}");
        params.put(ENVIRONMENT_USERNAME, "${bamboo.cf.username}");
        params.put(ENVIRONMENT_PASSWORD, "${bamboo.cf.password}");
        params.put(ENVIRONMENT_PROXY_HOST, "${bamboo.cf.proxy.host}");
        params.put(ENVIRONMENT_PROXY_PORT, "${bamboo.cf.proxy.port}");
        // password change triggers custom validation
        params.put(PASSWORD_CHANGE, "true");

        ActionParametersMap actionParametersMap = new SimpleActionParametersMap(params);

        BaseCloudFoundryTaskConfigurator taskConfigurator = new BaseCloudFoundryTaskConfigurator(adminService, textProvider, taskConfiguratorHelper, encryptionService, cloudFoundryServiceFactory);

        assertTaskValidationHasNoErrors(taskConfigurator, actionParametersMap, errorCollection);
    }

    @Test
    public void theFirstTimeATaskIsSavedIfPasswordContainsABambooVariableItIsNotEncryptedAndIsEncryptedFlagIsSetToFalse(){
        Map<String, Object> params = getBaseRequiredParameters();
        params.put(ENVIRONMENT_CONFIG_OPTION, ENVIRONMENT_CONFIG_OPTION_TASK);
        params.put(ENVIRONMENT_PASSWORD, "${bamboo.cf.password}");

        ActionParametersMap actionParametersMap = new SimpleActionParametersMap(params);

        BaseCloudFoundryTaskConfigurator taskConfigurator = new BaseCloudFoundryTaskConfigurator(adminService, textProvider, taskConfiguratorHelper, encryptionService, cloudFoundryServiceFactory);

        Map<String, String> taskConfigMap = taskConfigurator.generateTaskConfigMap(actionParametersMap, null);

        assertThat(taskConfigMap.get(ENVIRONMENT_PASSWORD_ENCRYPTED), is("${bamboo.cf.password}"));
        assertThat(taskConfigMap.get(ENVIRONMENT_IS_PASSWORD_ENCRYPTED), is("false"));
    }

    @Test
    public void theFirstTimeATaskIsSavedIfPasswordDoesNotContainABambooVariableItIsEncryptedAndIsEncryptedFlagIsSetToTrue(){
        // Given
        Map<String, Object> params = getBaseRequiredParameters();
        params.put(ENVIRONMENT_CONFIG_OPTION, ENVIRONMENT_CONFIG_OPTION_TASK);
        params.put(ENVIRONMENT_PASSWORD, "secret");

        ActionParametersMap actionParametersMap = new SimpleActionParametersMap(params);
        BaseCloudFoundryTaskConfigurator taskConfigurator = new BaseCloudFoundryTaskConfigurator(adminService, textProvider, taskConfiguratorHelper, encryptionService, cloudFoundryServiceFactory);

        when(encryptionService.encrypt("secret")).thenReturn("encrypted");

        // When
        Map<String, String> taskConfigMap = taskConfigurator.generateTaskConfigMap(actionParametersMap, null);

        // Then
        assertThat(taskConfigMap.get(ENVIRONMENT_PASSWORD_ENCRYPTED), is("encrypted"));
        assertThat(taskConfigMap.get(ENVIRONMENT_IS_PASSWORD_ENCRYPTED), is("true"));
    }

    @Test
    public void whenChangingThePasswordIfPasswordContainsABambooVariableItIsNotEncryptedAndIsEncryptedFlagIsSetToFalse(){
        Map<String, Object> params = getBaseRequiredParameters();
        params.put(ENVIRONMENT_CONFIG_OPTION, ENVIRONMENT_CONFIG_OPTION_TASK);
        params.put(ENVIRONMENT_PASSWORD, "${bamboo.cf.password}");
        params.put(PASSWORD_CHANGE, "true");

        ActionParametersMap actionParametersMap = new SimpleActionParametersMap(params);

        BaseCloudFoundryTaskConfigurator taskConfigurator = new BaseCloudFoundryTaskConfigurator(adminService, textProvider, taskConfiguratorHelper, encryptionService, cloudFoundryServiceFactory);

        Map<String, String> taskConfigMap = taskConfigurator.generateTaskConfigMap(actionParametersMap, previousTaskDefinition);

        assertThat(taskConfigMap.get(ENVIRONMENT_PASSWORD_ENCRYPTED), is("${bamboo.cf.password}"));
        assertThat(taskConfigMap.get(ENVIRONMENT_IS_PASSWORD_ENCRYPTED), is("false"));
    }

    @Test
    public void whenChangingThePasswordIfPasswordDoesNotContainABambooVariableItIsEncryptedAndIsEncryptedFlagIsSetToTrue(){
        // Given
        Map<String, Object> params = getBaseRequiredParameters();
        params.put(ENVIRONMENT_CONFIG_OPTION, ENVIRONMENT_CONFIG_OPTION_TASK);
        params.put(ENVIRONMENT_PASSWORD, "secret");
        params.put(PASSWORD_CHANGE, "true");

        ActionParametersMap actionParametersMap = new SimpleActionParametersMap(params);
        BaseCloudFoundryTaskConfigurator taskConfigurator = new BaseCloudFoundryTaskConfigurator(adminService, textProvider, taskConfiguratorHelper, encryptionService, cloudFoundryServiceFactory);

        when(encryptionService.encrypt("secret")).thenReturn("encrypted");

        // When
        Map<String, String> taskConfigMap = taskConfigurator.generateTaskConfigMap(actionParametersMap, previousTaskDefinition);

        // Then
        assertThat(taskConfigMap.get(ENVIRONMENT_PASSWORD_ENCRYPTED), is("encrypted"));
        assertThat(taskConfigMap.get(ENVIRONMENT_IS_PASSWORD_ENCRYPTED), is("true"));
    }
}
