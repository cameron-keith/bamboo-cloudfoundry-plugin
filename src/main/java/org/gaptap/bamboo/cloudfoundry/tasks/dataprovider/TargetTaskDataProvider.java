/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.tasks.dataprovider;

import com.atlassian.bamboo.serialization.WhitelistedSerializable;
import com.atlassian.bamboo.task.RuntimeTaskDataProvider;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.runtime.RuntimeTaskDefinition;
import com.atlassian.bamboo.v2.build.CommonContext;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import org.apache.commons.lang.StringUtils;
import org.gaptap.bamboo.cloudfoundry.admin.CloudFoundryAdminService;
import org.gaptap.bamboo.cloudfoundry.admin.Proxy;
import org.gaptap.bamboo.cloudfoundry.admin.Target;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

import static org.gaptap.bamboo.cloudfoundry.tasks.config.BaseCloudFoundryTaskConfigurator.TARGET_ID;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.BaseCloudFoundryTaskConfigurator.ENVIRONMENT_CONFIG_OPTION;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.BaseCloudFoundryTaskConfigurator.ENVIRONMENT_CONFIG_OPTION_TASK;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.BaseCloudFoundryTaskConfigurator.ENVIRONMENT_CONFIG_OPTION_SHARED;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.BaseCloudFoundryTaskConfigurator.ENVIRONMENT_IS_PASSWORD_ENCRYPTED;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.BaseCloudFoundryTaskConfigurator.ENVIRONMENT_URL;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.BaseCloudFoundryTaskConfigurator.ENVIRONMENT_USERNAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.BaseCloudFoundryTaskConfigurator.ENVIRONMENT_PASSWORD_ENCRYPTED;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.BaseCloudFoundryTaskConfigurator.ENVIRONMENT_TRUST_CERTS;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.BaseCloudFoundryTaskConfigurator.ENVIRONMENT_PROXY_HOST;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.BaseCloudFoundryTaskConfigurator.ENVIRONMENT_PROXY_PORT;

/**
 * Provides centrally stored Target data to the Task.
 *
 * @author David Ehringer
 */
public class TargetTaskDataProvider implements RuntimeTaskDataProvider {

    public static final String TARGET_URL = "targetUrl";
    public static final String TRUST_SELF_SIGNED_CERTS = "trustSelfSignedCerts";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String IS_PASSWORD_ENCRYPTED = "isPasswordEncrypted";
    public static final String PROXY_HOST = "proxyHost";
    public static final String PROXY_PORT = "proxyPort";
    public static final String DISABLE_FOR_BUILD_PLANS = "disableForBuildPlans";

    private CloudFoundryAdminService adminService;
    private TransactionTemplate transactionTemplate;

    public TargetTaskDataProvider(CloudFoundryAdminService adminService, TransactionTemplate transactionTemplate) {
        this.adminService = adminService;
        this.transactionTemplate = transactionTemplate;
    }

    @Override
    @NotNull
    public Map<String, String> populateRuntimeTaskData(@NotNull TaskDefinition taskDefinition,
                                                       @NotNull CommonContext commonContext) {

        Map<String, String> config = taskDefinition.getConfiguration();
        String environmentConfigOption = config.get(ENVIRONMENT_CONFIG_OPTION);
        // isEmpty handles tasks configured prior to ENVIRONMENT_CONFIG_OPTION existing and there was only the shared config option
        if(StringUtils.isEmpty(environmentConfigOption) || ENVIRONMENT_CONFIG_OPTION_SHARED.equals(environmentConfigOption)) {
            final String targetId = taskDefinition.getConfiguration().get(TARGET_ID);
            return transactionTemplate.execute(new TransactionCallback<Map<String, String>>() {
                @Override
                public Map<String, String> doInTransaction() {
                    Map<String, String> data = new HashMap<String, String>();
                    Target target = adminService.getTarget(Integer.parseInt(targetId));

                    data.put(TARGET_URL, target.getUrl());
                    data.put(TRUST_SELF_SIGNED_CERTS, String.valueOf(target.isTrustSelfSignedCerts()));
                    data.put(USERNAME, target.getCredentials().getUsername());
                    data.put(PASSWORD, target.getCredentials().getPassword());
                    data.put(IS_PASSWORD_ENCRYPTED, "true");
                    data.put(DISABLE_FOR_BUILD_PLANS, String.valueOf(target.isDisableForBuildPlans()));

                    Proxy proxy = target.getProxy();
                    if (proxy != null) {
                        data.put(PROXY_HOST, proxy.getHost());
                        data.put(PROXY_PORT, String.valueOf(proxy.getPort()));
                    }
                    return data;
                }
            });
        } else if (ENVIRONMENT_CONFIG_OPTION_TASK.equals(environmentConfigOption)) {
            Map<String, String> data = new HashMap<String, String>();
            data.put(TARGET_URL, config.get(ENVIRONMENT_URL));
            data.put(TRUST_SELF_SIGNED_CERTS, config.get(ENVIRONMENT_TRUST_CERTS));
            data.put(USERNAME, config.get(ENVIRONMENT_USERNAME));
            data.put(PASSWORD, config.get(ENVIRONMENT_PASSWORD_ENCRYPTED));
            data.put(IS_PASSWORD_ENCRYPTED, config.get(ENVIRONMENT_IS_PASSWORD_ENCRYPTED));
            data.put(DISABLE_FOR_BUILD_PLANS, "false");

            if (StringUtils.isNotBlank(config.get(ENVIRONMENT_PROXY_HOST))) {
                data.put(PROXY_HOST, config.get(ENVIRONMENT_PROXY_HOST));
                data.put(PROXY_PORT, config.get(ENVIRONMENT_PROXY_PORT));
            }

            return data;
        } else {
            throw new IllegalArgumentException("Unknown ENVIRONMENT_CONFIG_OPTION value: " + environmentConfigOption);
        }
    }

    @Override
    public void processRuntimeTaskData(@NotNull TaskDefinition taskDefinition, @NotNull CommonContext commonContext) {
        // Nothing to do
    }

    @Override
    public void processRuntimeTaskData(@NotNull RuntimeTaskDefinition taskDefinition, @NotNull CommonContext commonContext) {
        // Nothing to do
    }

    @Override
    public Map<String, WhitelistedSerializable> createRuntimeTaskData(@NotNull RuntimeTaskDefinition taskDefinition, @NotNull CommonContext commonContext) {
        return new HashMap<String, WhitelistedSerializable>();
    }
}
