package org.gaptap.bamboo.cloudfoundry.tasks;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.security.EncryptionService;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import org.apache.commons.lang.StringUtils;
import org.cloudfoundry.client.v3.tasks.CreateTaskResponse;
import org.gaptap.bamboo.cloudfoundry.client.CloudFoundryService;

import java.util.Map;
import java.util.concurrent.CountDownLatch;

import static org.gaptap.bamboo.cloudfoundry.tasks.config.TaskTaskConfigurator.OPTION_RUN_TASK;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.TaskTaskConfigurator.OPTION_WAIT_ON_TASK;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.TaskTaskConfigurator.RUN_APPLICATION_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.TaskTaskConfigurator.RUN_CAPTURE_TASK_ID;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.TaskTaskConfigurator.RUN_COMMAND;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.TaskTaskConfigurator.RUN_ENVIRONMENT;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.TaskTaskConfigurator.RUN_MEMORY;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.TaskTaskConfigurator.RUN_TASK_ID_VARIABLE_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.TaskTaskConfigurator.RUN_TASK_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.TaskTaskConfigurator.SELECTED_OPTION;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.TaskTaskConfigurator.WAIT_APPLICATION_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.TaskTaskConfigurator.WAIT_FAIL_ON_TASK_FAILURE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.TaskTaskConfigurator.WAIT_TASK_ID;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.TaskTaskConfigurator.WAIT_TERMINATE_TASK_ON_TIMEOUT;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.TaskTaskConfigurator.WAIT_TIMEOUT;
import static org.gaptap.bamboo.cloudfoundry.tasks.utils.ApplicationConfigurationMapper.parseEnvironmentList;

public class TaskTask extends AbstractCloudFoundryTask {

    public TaskTask(EncryptionService encryptionService) {
        super(encryptionService);
    }

    @Override
    protected TaskResult doExecute(CommonTaskContext taskContext) throws TaskException {
        BuildLogger buildLogger = taskContext.getBuildLogger();
        TaskResultBuilder taskResultBuilder = TaskResultBuilder.newBuilder(taskContext).success();

        ConfigurationMap configMap = taskContext.getConfigurationMap();
        String option = configMap.get(SELECTED_OPTION);

        CloudFoundryService cloudFoundry = getCloudFoundryService(taskContext);
        try {
            if (OPTION_RUN_TASK.equals(option)) {
                buildLogger.addBuildLogEntry(String.format("Creating task for app %s %s", configMap.get(RUN_APPLICATION_NAME), getLoginContext(taskContext)));
                runTask(cloudFoundry, buildLogger, configMap, taskResultBuilder, taskContext);
            } else if (OPTION_WAIT_ON_TASK.equals(option)) {
                buildLogger.addBuildLogEntry(String.format("Waiting %,d seconds for one-off task %s to complete %s", configMap.getAsLong(WAIT_TIMEOUT), configMap.get(WAIT_TASK_ID), getLoginContext(taskContext)));
                waitOnTask(cloudFoundry, buildLogger, configMap, taskResultBuilder);
            } else {
                throw new TaskException("Unknown or unspecified task management option: " + option);
            }
        } catch (InterruptedException e) {
            buildLogger.addErrorLogEntry("Unable to complete task due to unknown error: " + e.getMessage());
            taskResultBuilder.failedWithError();
        }
        return taskResultBuilder.build();
    }

    private void runTask(CloudFoundryService cloudFoundry, BuildLogger buildLogger, ConfigurationMap configMap, TaskResultBuilder taskResultBuilder, CommonTaskContext taskContext) throws InterruptedException {
        doSubscribe(cloudFoundry
                        .runTask(configMap.get(RUN_APPLICATION_NAME),
                                configMap.get(RUN_COMMAND),
                                configMap.get(RUN_TASK_NAME),
                                toInteger(configMap.get(RUN_MEMORY)),
                                mapFromString(configMap.get(RUN_ENVIRONMENT)))
                        .filter(task -> configMap.getAsBoolean(RUN_CAPTURE_TASK_ID))
                        .map(task -> {
                            String name = configMap.get(RUN_TASK_ID_VARIABLE_NAME);
                            taskContext.getCommonContext().getVariableContext().addResultVariable(name, task.getId());
                            return true;
                        })
                        .then(),
                "Unable to create one-off task.", buildLogger, taskResultBuilder);
    }

    private void waitOnTask(CloudFoundryService cloudFoundry, BuildLogger buildLogger, ConfigurationMap configMap, TaskResultBuilder taskResultBuilder) throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        cloudFoundry
                .waitForTaskCompletion(
                        configMap.get(WAIT_APPLICATION_NAME),
                        configMap.get(WAIT_TASK_ID),
                        toInteger(configMap.get(WAIT_TIMEOUT)),
                        Boolean.valueOf(configMap.get(WAIT_TERMINATE_TASK_ON_TIMEOUT)))
                .subscribe(isCompleted -> {},
                throwable -> {
                    if(throwable instanceof IllegalStateException && configMap.getAsBoolean(WAIT_FAIL_ON_TASK_FAILURE)){
                        taskResultBuilder.failedWithError();
                    }
                    buildLogger.addErrorLogEntry("Unable to wait for completion of one-off task: " + throwable.getMessage());
                    latch.countDown();
                }, latch::countDown
        );
        latch.await();
    }

    private Integer toInteger(String string) {
        if(StringUtils.isBlank(string)){
            return null;
        }
        return Integer.parseInt(string);
    }

    private Map<String, String> mapFromString(String string){
        if(StringUtils.isBlank(string)){
            return null;
        }
        return parseEnvironmentList(string);
    }
}
