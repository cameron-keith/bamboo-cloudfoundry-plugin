/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.admin;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Iterables.filter;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.log4j.Logger;

import static org.gaptap.bamboo.cloudfoundry.admin.CloudFoundryPredicates.*;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.bamboo.build.Job;
import com.atlassian.sal.api.transaction.TransactionCallback;

import org.gaptap.bamboo.cloudfoundry.admin.actions.ManageTargetsAction;
import org.gaptap.bamboo.cloudfoundry.admin.tasks.CloudFoundryTaskConfigurationService;

import com.google.common.collect.Lists;

/**
 * @author David Ehringer
 */
public class DefaultCloudFoundryAdminService implements CloudFoundryAdminService {

	private static final Logger LOG = Logger.getLogger(ManageTargetsAction.class);

	private final ActiveObjects ao;
	private final CloudFoundryTaskConfigurationService taskConfigurationService;

	public DefaultCloudFoundryAdminService(ActiveObjects ao,
			CloudFoundryTaskConfigurationService taskConfigurationService) {
		this.ao = ao;
		this.taskConfigurationService = taskConfigurationService;
	}

	@Override
	public Target addTarget(final String name, final String url, final String description, final boolean trustSelfSignedCerts, final int credentialsId,
			final Integer proxyId, final boolean disableForBuildPlans) {
		LOG.debug("Adding new Target with name " + name);
		return ao.executeInTransaction(new TransactionCallback<Target>() {
			@Override
			public Target doInTransaction() {
				Target target = ao.create(Target.class);
				overlayUpdates(target, name, url, description, trustSelfSignedCerts, credentialsId, proxyId, disableForBuildPlans);
				target.save();
				return target;
			}
		});
	}

	@Override
	public Target updateTarget(final int id, final String name, final String url, final String description, final boolean trustSelfSignedCerts,
			final int credentialsId, final Integer proxyId, final boolean disableForBuildPlans) {
		return ao.executeInTransaction(new TransactionCallback<Target>() {
			@Override
			public Target doInTransaction() {
				Target target = getTarget(id);
				// TODO not found condition
				overlayUpdates(target, name, url, description, trustSelfSignedCerts, credentialsId, proxyId, disableForBuildPlans);
				target.save();
				return target;
			}
		});
	}

	private void overlayUpdates(Target target, String name, String url, String description, boolean trustSelfSignedCerts, int credentialsId,
			Integer proxyId, boolean disableForBuildPlans) {
		Credentials credentials = getCredentials(credentialsId);
		Proxy proxy = null;
		if (proxyId != null) {
			proxy = getProxy(proxyId);
		}
		target.setName(name);
		target.setUrl(url);
		target.setDescription(description);
		target.setTrustSelfSignedCerts(trustSelfSignedCerts);
		target.setCredentials(credentials);
		target.setProxy(proxy);
		target.setDisableForBuildPlans(disableForBuildPlans);
	}

	@Override
	public Target getTarget(int id) {
		return ao.get(Target.class, id);
	}

	@Override
	public List<Target> allTargets() {
        List<Target> targets = newArrayList(ao.find(Target.class));
        Collections.sort(targets, new Comparator<Target>() {
            @Override
            public int compare(Target one, Target two) {
                return String.CASE_INSENSITIVE_ORDER.compare(one.getName(), two.getName());
            }
        });
        return targets;
	}

	@Override
	public void deleteTarget(final int id) {
		ao.executeInTransaction(new TransactionCallback<Void>() {
			@Override
			public Void doInTransaction() {
				Target target = getTarget(id);
				// TODO change this to look at Jobs and Deployment tasks
				List<Job> jobsUsingTarget = taskConfigurationService.allJobsUsingTarget(target);
				if (!jobsUsingTarget.isEmpty()) {
					throw new InUseByJobException(jobsUsingTarget);
				}
				ao.delete(target);
				return null;
			}
		});
	}

	@Override
	public Collection<Credentials> allCredentials() {
        List<Credentials> credentials = newArrayList(ao.find(Credentials.class));
        Collections.sort(credentials, new Comparator<Credentials>() {
            @Override
            public int compare(Credentials one, Credentials two) {
                return String.CASE_INSENSITIVE_ORDER.compare(one.getName(), two.getName());
            }
        });
        return credentials;
	}

	@Override
	public Credentials addCredentials(final String name, final String username, final String password,
			final String description) {
		return ao.executeInTransaction(new TransactionCallback<Credentials>() {
			@Override
			public Credentials doInTransaction() {
				Credentials credentials = ao.create(Credentials.class);
				overlayUpdates(credentials, name, username, password, description);
				credentials.save();
				return credentials;
			}
		});
	}

	@Override
	public Credentials getCredentials(int id) {
		return ao.get(Credentials.class, id);
	}

	@Override
	public Credentials updateCredentials(final int id, final String name, final String username, final String password,
			final String description) {
		return ao.executeInTransaction(new TransactionCallback<Credentials>() {
			@Override
			public Credentials doInTransaction() {
				// TODO not found condition
				Credentials credentials = getCredentials(id);
				overlayUpdates(credentials, name, username, password, description);
				credentials.save();
				return credentials;
			}
		});
	}

	private void overlayUpdates(Credentials credentials, String name, String username, String password,
			String description) {
		credentials.setName(name);
		credentials.setUsername(username);
		credentials.setPassword(password);
		credentials.setDescription(description);
	}

	@Override
	public void deleteCredentials(final int id) {
		ao.executeInTransaction(new TransactionCallback<Void>() {
			@Override
			public Void doInTransaction() {
				Credentials credentials = getCredentials(id);
				List<Target> targetsUsingCredentials = allTargetsUsing(credentials);
				if (!targetsUsingCredentials.isEmpty()) {
					throw new InUseByTargetException(targetsUsingCredentials);
				}
				ao.delete(credentials);
				return null;
			}
		});
	}

	private List<Target> allTargetsUsing(Credentials credentials) {
		return Lists.newArrayList(filter(allTargets(), targetsUsing(credentials)));
	}

	@Override
	public Collection<Proxy> allProxies() {
		return newArrayList(ao.find(Proxy.class));
	}

	@Override
	public Proxy addProxy(final String name, final String host, final int port, final String description) {
		return ao.executeInTransaction(new TransactionCallback<Proxy>() {
			@Override
			public Proxy doInTransaction() {
				Proxy proxy = ao.create(Proxy.class);
				overlayUpdates(proxy, name, host, port, description);
				proxy.save();
				return proxy;
			}
		});
	}

	@Override
	public Proxy getProxy(int id) {
		return ao.get(Proxy.class, id);
	}

	private void overlayUpdates(Proxy proxy, String name, String host, int port, String description) {
		proxy.setName(name);
		proxy.setHost(host);
		proxy.setPort(port);
		proxy.setDescription(description);
	}

	@Override
	public Proxy updateProxy(final int id, final String name, final String host, final int port,
			final String description) {
		return ao.executeInTransaction(new TransactionCallback<Proxy>() {
			@Override
			public Proxy doInTransaction() {
				Proxy proxy = getProxy(id);
				overlayUpdates(proxy, name, host, port, description);
				proxy.save();
				return proxy;
			}
		});
	}

	private List<Target> allTargetsUsing(Proxy proxy) {
		return Lists.newArrayList(filter(allTargets(), targetsUsing(proxy)));
	}

	@Override
	public void deleteProxy(final int proxyId) {
		ao.executeInTransaction(new TransactionCallback<Void>() {
			@Override
			public Void doInTransaction() {
				Proxy proxy = getProxy(proxyId);
				List<Target> targetsUsingProxy = allTargetsUsing(proxy);
				if (!targetsUsingProxy.isEmpty()) {
					throw new InUseByTargetException(targetsUsingProxy);
				}
				ao.delete(proxy);
				return null;
			}
		});
	}
}
