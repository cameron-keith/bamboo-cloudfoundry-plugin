/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author David Ehringer
 */
public class PluginProperties {
	
	private static final PluginProperties INSTANCE = new PluginProperties();
	private static final String DEFAULT_VALUE = "";

	private final Properties properties = new Properties();

	private PluginProperties() {
		final InputStream input = getClass().getResourceAsStream("plugin.properties");
		try {
			properties.load(input);
		} catch (IOException e) {
			throw new RuntimeException("Failed to load plugin properties", e);
		}
	}
	
	public static String getPluginKey(){
		return INSTANCE.properties.getProperty("plugin.key", DEFAULT_VALUE);
	}
	
	public static String getPushTaskKey(){
		return INSTANCE.properties.getProperty("plugin.task.push.key", DEFAULT_VALUE);
	}
	
	public static String getServiceTaskKey(){
		return INSTANCE.properties.getProperty("plugin.task.service.key", DEFAULT_VALUE);
	}
	
	public static String getApplicationTaskKey(){
		return INSTANCE.properties.getProperty("plugin.task.application.key", DEFAULT_VALUE);
	}
    
    public static String getRoutingTaskKey(){
        return INSTANCE.properties.getProperty("plugin.task.routing.key", DEFAULT_VALUE);
    }
}
